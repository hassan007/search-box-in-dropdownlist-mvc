﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Searchbox_in_Dropdownlist.Controllers
{
    public class ErrorsController : Controller
    {
        // GET: Errors
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Http404(string error)
        {
            ViewData["Title"] = "Sorry, an error occurred while processing your request. (404)";
            ViewBag.D = error;
            return View("Http404");
        }
        public ActionResult general(string error)
        {
           ViewBag.Title = "Sorry, an error occurred while processing your request.";
            ViewBag.D = error;
            return View("general");
        }
    }
}