﻿using System.Web;
using System.Web.Mvc;

namespace Searchbox_in_Dropdownlist
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
